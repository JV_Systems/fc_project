from flask import render_template
from ..models import Employee

from . import home


@home.route('')
def load_dashboard():
    data = Employee.query.all()
    return render_template('home.html', data=data)
