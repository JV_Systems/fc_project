from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_bcrypt import Bcrypt
from flask_cors import CORS


db = SQLAlchemy()
ma = Marshmallow()
bcrypt = Bcrypt()
cors = CORS()


def create_app():
    app = Flask(__name__)
    app.config.from_object('config')
    db.init_app(app)
    ma.init_app(app)
    cors.init_app(app, resources={r"*": {"origins": "*"}})

    with app.app_context():
        # load all tables from DB
        db.Model.metadata.reflect(bind=db.engine, schema='rezervat_db_prod')

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint, url_prefix='/')

    return app