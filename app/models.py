from datetime import datetime
from random import randint

from . import bcrypt
from sqlalchemy.orm import relationship
from . import db, ma


class Customer(db.Model):
    """deal with an existing customer table"""
    __table__ = db.Model.metadata.tables['rezervat_db_prod.re_customer']

    def __init__(self, request_data, empl):
        self.name = request_data['name']
        self.surname = request_data['surname']
        self.email = request_data['email']
        self.telephone = request_data['telephone']
        self.status = request_data['status']
        self.internal_desc = request_data['internal_desc']
        self.created_date = datetime.now()
        self.modified_date = datetime.now()
        self.created_by = empl.name + " " + empl.surname
        self.modified_by = empl.name + " " + empl.surname


class CustomerSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Customer


customer_schema = CustomerSchema()
customers_schema = CustomerSchema(many=True)


class Service(db.Model):
    """deal with an existing service table"""
    __table__ = db.Model.metadata.tables['rezervat_db_prod.re_service']

    def __init__(self, request_data, empl_email):
        self.name = request_data['name']
        self.description = request_data['description']
        self.service_time = request_data['service_time']
        self.service_price = request_data['service_price']
        self.status = request_data['status']
        self.created_date = datetime.now()
        self.modified_date = datetime.now()
        self.created_by = empl_email
        self.modified_by = empl_email


class ServiceSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Service


service_schema = ServiceSchema()
services_schema = ServiceSchema(many=True)
services_os_schema = ServiceSchema(many=True, only=['id', 'name', 'service_price', 'service_time', 'description'])


class Employee(db.Model):
    """deal with an existing employee table"""
    __table__ = db.Model.metadata.tables['rezervat_db_prod.re_employee']

    def __init__(self, request_data, empl):
        self.name = request_data['name']
        self.surname = request_data['surname']
        self.email = request_data['email']
        self.show_at_form = True
        self.password = self.hash_password(request_data['password'])
        self.status = request_data['status']
        self.internal_desc = request_data['internal_desc']
        self.external_desc = request_data['external_desc']
        self.created_date = datetime.now()
        self.modified_date = datetime.now()
        self.created_by = empl.name + " " + empl.surname
        self.modified_by = empl.name + " " + empl.surname

    def validate_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def hash_password(self, password):
        return bcrypt.generate_password_hash(password).decode('utf-8')


class EmployeeSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Employee


employee_schema = EmployeeSchema()
employees_schema = EmployeeSchema(many=True)


class Action(db.Model):
    """deal with an existing action table"""
    __table__ = db.Model.metadata.tables['rezervat_db_prod.re_actions']
    employee = relationship("Employee")
    service = relationship("Service")


class ActionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Action
    employee = ma.Nested(EmployeeSchema)
    service = ma.Nested(ServiceSchema)


action_schema = ActionSchema()
actions_schema = ActionSchema(many=True)


class Term(db.Model):
    """deal with an existing term table"""
    __table__ = db.Model.metadata.tables['rezervat_db_prod.re_term']
    employee = relationship("Employee", backref="re_employee")

    def __init__(self, *args):
        if len(args) > 1:
            term_req = args[0]
            empl = args[1]
            self.employee_id = empl.id
            self.description = "Created by employee"
            self.working_date = datetime.strptime(term_req['working_date'], '%Y-%m-%d').date()
            self.time_from = datetime.strptime(term_req['time_from'], '%H:%M:%S').time()
            self.time_to = datetime.strptime(term_req['time_to'], '%H:%M:%S').time()
            self.created_date = datetime.now()
            self.modified_date = datetime.now()
            self.created_by = empl.email
            self.modified_by = empl.email
        else:
            empl = args[0]
            self.employee_id = empl.id
            self.description = "Create by rezervathor"
            self.created_date = datetime.now()
            self.modified_date = datetime.now()
            self.created_by = empl.email
            self.modified_by = empl.email


class TermSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Term
    employee = ma.Nested(EmployeeSchema)


term_schema = TermSchema()
terms_schema = TermSchema(many=True)
terms_os_schema = TermSchema(many=True, only=['working_date'])


class Order(db.Model):
    """deal with an existing order table"""
    __table__ = db.Model.metadata.tables['rezervat_db_prod.re_order']
    employee = relationship("Employee")
    service = relationship("Service")
    customer = relationship("Customer")

    def __init__(self, req_data, empl):
        self.employee_id = req_data['employee_id']
        self.service_id = req_data['service_id']
        self.customer_id = req_data['customer_id']
        self.status = 'A'
        self.order_password = self.generate_random_password()
        self.internal_desc = req_data['internal_desc']
        self.work_date = datetime.strptime(req_data['work_date'], "%Y-%m-%dT%H:%M:%S.%fZ").date()
        self.date_from = datetime.strptime(req_data['date_from'], "%Y-%m-%dT%H:%M:%S.%fZ")
        self.date_to = datetime.strptime(req_data['date_to'], "%Y-%m-%dT%H:%M:%S.%fZ")
        self.created_date = datetime.now()
        self.modified_date = datetime.now()
        self.created_by = empl.name + " " + empl.surname
        self.modified_by = empl.name + " " + empl.surname

    def generate_random_password(self):
        final_string = ""
        for i in range(8):
            final_string += str(randint(1, 9))
        return final_string


class OrderSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Order
    employee = ma.Nested(EmployeeSchema)
    service = ma.Nested(ServiceSchema)
    customer = ma.Nested(CustomerSchema)


order_schema = OrderSchema()
orders_schema = OrderSchema(many=True)
orders_os_schema = OrderSchema(many=True, only=['date_from', 'date_to'])


class WorkingHour(db.Model):
    """deal with an existing term table"""
    __table__ = db.Model.metadata.tables['rezervat_db_prod.re_working_hours']
    employee = relationship("Employee")

    def __init__(self, term_req, empl_ide, empl):
        self.employee_id = empl_ide
        self.auto_create = term_req['auto_create']
        self.day = term_req['day']
        self.time_from = datetime.strptime(term_req['time_from'], '%H:%M').time()
        self.time_to = datetime.strptime(term_req['time_to'], '%H:%M').time()
        self.created_date = datetime.now()
        self.modified_date = datetime.now()
        self.created_by = empl.name + " " + empl.surname
        self.modified_by = empl.name + " " + empl.surname


class WorkingHourSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = WorkingHour
    employee = ma.Nested(EmployeeSchema)


workingHour_schema = WorkingHourSchema()
workingHours_schema = WorkingHourSchema(many=True)


class ServiceUsedForEmployee(db.Model):
    """deal with an existing table"""
    __table__ = db.Model.metadata.tables['rezervat_db_prod.re_service_used_for_employee']
    employee = relationship("Employee")
    service = relationship("Service")

    def __init__(self, ser_id, new_empl_ide, empl):
        self.employee_id = new_empl_ide
        self.service_id = ser_id
        self.valid_from = datetime.now().date()
        self.valid_to = datetime.strptime("4000-01-01", '%Y-%m-%d').date()
        self.created_date = datetime.now()
        self.modified_date = datetime.now()
        self.created_by = empl.name + " " + empl.surname
        self.modified_by = empl.name + " " + empl.surname


class ServiceUsedForEmployeeSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ServiceUsedForEmployee
    employee = ma.Nested(EmployeeSchema)
    service = ma.Nested(ServiceSchema)


serviceUsedForEmployee_schema = ServiceUsedForEmployeeSchema()
serviceUsedForEmployees_schema = ServiceUsedForEmployeeSchema(many=True)
serviceUsedForEmployees_os_schema = ServiceUsedForEmployeeSchema(many=True, only=['employee.id', 'employee.name'])
